Using YoloV3 to detect welds
==========================

Modified code from: https://github.com/zzh8829/yolov3-tf2/

Modifications to original code include: Auto extract of bounding boxes, 
Generate additional images with data augmentation, modified the training file 
to be a one class classifier,...
## Installation
Clone directory to local folder and navigate to folder via the terminal.

Install the necessary requirements with following command:
```bash
pip install -r requirements.txt
```
For GPU support, follow the steps on: https://www.tensorflow.org/install/gpu
## Data Workflow

### 1. Create dataset
#### 1.1 Annotation
Annotate the video file of weld video using Microsoft VoTT https://github.com/microsoft/VoTT 

**VOTT Settings**
* Project Settings:

        Display Name: Lasnaden
        
* Export settings:

      Provider:Pascal VOC  
      Asset State: Only tagged assets
      Test/Train Split: 80-20

* IMPORTANT: Labels in VOTT cannot contain spaces. So instead of "weld ok" change label to "weld_ok"

#### 1.2 Data Augmentation
In order to create a more robust neural network, extra images are generated with following data augmentation techniques:
* PCA Color Augmentation
* horizontal and vertical flip
* Three random rotations

The data_aug.py script creates the augmented images and also the .xml files with modified bounding boxes (if needed)
Other files are created/updated that serve as input for the next functions. 
These files are: bb_box.txt, classes.names and file_path
```bash
python data_aug.py --VOC_data ./data/lasnaden_tank-PascalVOC-export/
```

#### 1.3 Export to VOC format


Export to VOC format and convert to tfrecords via voc_las.py
```bash
python tools/voc_las.py --VOC_data ./data/lasnaden_tank-PascalVOC-export/ --split val
python tools/voc_las.py --VOC_data ./data/lasnaden_tank-PascalVOC-export/ --split train 
```

and check if the database is loaded correct:
```bash
python tools/visualize_dataset.py --VOC_data ./data/lasnaden_tank-PascalVOC-export/
```

### 2. Training
Download the previously trained darknet weights to start with. And convert them to yolov3 for tensorflow v2

```bash
Linux:
wget https://pjreddie.com/media/files/yolov3.weights -O data/yolov3.weights
Windows:
curl https://pjreddie.com/media/files/yolov3.weights

python convert.py
python detect.py --image ./data/meme.jpg --classes ./data/coco.names # Sanity check on coco database

python train.py --VOC_data ./data/lasnaden_tank-PascalVOC-export/ --num_classes 2 --mode fit --transfer darknet --batch_size 1 --epochs 100 --weights_num_classes 80

```
The detect.py script can be used to evaluate one image.

detect_video.py is used to annotate a video file.



```bash
python detect.py --image ./val_las.jpg --classes ./data/voc_lasnaden.names --weights ./checkpoints/yolov3_train_17.tf --num_classes 2
python detect_video.py --video video.mp4 --classes ./data/lasnaden_tank-PascalVOC-export/classes.names --weights ./checkpoints/yolov3_train_22.tf --num_classes 2 --output ./output.avi
python detect_video.py --video cut_output.mp4 --classes ./data/lasnaden_tank-PascalVOC-export/classes.names --weights ./checkpoints/yolov3_train_14.tf --num_classes 2 --output ./output.avi
```
