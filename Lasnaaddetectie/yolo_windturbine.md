pad naar dataset: 
C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export

python data_aug.py --VOC_data "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export"

python tools/voc_las.py --VOC_data "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export" --split val
python tools/voc_las.py --VOC_data "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export" --split train

python tools/visualize_dataset.py --VOC_data "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export"
 
 
python detect.py --image ./data/meme.jpg # Sanity check --classes ./data/coco.names
RD /S "./logs/"
python train.py --VOC_data "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export" --num_classes 5 --mode fit --transfer darknet --batch_size 3 --epochs 100 --weights_num_classes 80

python detect.py --classes "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export\classes.names" --weights ./checkpoints/yolov3_train_100.tf --image "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\image21.jpg" --num_classes 5
python detect_folder.py --classes "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export\classes.names" --weights ./checkpoints/yolov3_train_100.tf --folder "C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 3\" --num_classes 5
