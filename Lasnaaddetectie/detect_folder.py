import time
from absl import app, flags, logging
from absl.flags import FLAGS
import cv2
import re
import numpy as np
import tensorflow as tf
from yolov3_tf2.models import (
    YoloV3, YoloV3Tiny
)
from yolov3_tf2.dataset import transform_images, load_tfrecord_dataset
from yolov3_tf2.utils import draw_outputs

flags.DEFINE_string('classes',
                    'C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 1\EngieWindturbine-PascalVOC-export\classes.names',
                    'path to classes file')
flags.DEFINE_string('weights', './checkpoints/yolov3_train_100.tf',
                    'path to weights file')
flags.DEFINE_boolean('tiny', False, 'yolov3 or yolov3-tiny')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string('folder', 'C:\Datasets\wetransfer-efa45d(1)\WT3 - Blade 2', 'path to folder')
flags.DEFINE_string('tfrecord', None, 'tfrecord instead of image')
flags.DEFINE_integer('num_classes', 5, 'number of classes in the model')


def main(_argv):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)
    import os
    if FLAGS.tiny:
        yolo = YoloV3Tiny(classes=FLAGS.num_classes)
    else:
        yolo = YoloV3(classes=FLAGS.num_classes)

    yolo.load_weights(FLAGS.weights).expect_partial()
    logging.info('weights loaded')

    class_names = [c.strip() for c in open(FLAGS.classes).readlines()]
    logging.info('classes loaded')
    outputfolder = os.path.join(FLAGS.folder,'predictions')
    if not os.path.exists(outputfolder):
        os.makedirs(outputfolder)

    jpgfiles = [f for f in os.listdir(FLAGS.folder) if re.match(r'.*\.jpg|.*\.JPG', f)]

    for loaded_image in jpgfiles:
        logging.info('image:' + loaded_image + 'loaded')
        img_path = os.path.join(FLAGS.folder, loaded_image)

        img_raw = tf.image.decode_image(
            open(img_path, 'rb').read(), channels=3)

        img = tf.expand_dims(img_raw, 0)
        img = transform_images(img, FLAGS.size)

        t1 = time.time()
        boxes, scores, classes, nums = yolo(img)
        print(classes)
        t2 = time.time()
        logging.info('time: {}'.format(t2 - t1))

        logging.info('detections:')
        for i in range(nums[0]):
            logging.info('\t{}, {}, {}'.format(class_names[int(classes[0][i])],
                                               np.array(scores[0][i]),
                                               np.array(boxes[0][i])))

        img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)
        img = draw_outputs(img, (boxes, scores, classes, nums), class_names)

        output_image = loaded_image.split('.')[0] + "_output.jpg"
        output_image_path = os.path.join(outputfolder,  output_image)
        cv2.imwrite(output_image_path, img)
        logging.info('output saved to: {}'.format(output_image))

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
