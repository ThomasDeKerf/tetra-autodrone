import time
import threading
from NeuralNetwork import *
from kivy.app import App
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.popup import Popup
from kivy.properties import ObjectProperty, StringProperty
from io import StringIO
from kivy.clock import Clock, mainthread
import webbrowser
## Imports to make pyinstaller work
import sklearn.utils._cython_blas
import sklearn.neighbors.typedefs
import sklearn.neighbors.quad_tree
import sklearn.tree
import sklearn.tree._utils


# TODO: number of trainable parameters

class FileChoosePopup(Popup):
    load = ObjectProperty()


class Tab(TabbedPanel):
    the_popup = ObjectProperty(None)

    def open_popup(self):
        self.the_popup = FileChoosePopup(load=self.load)
        self.the_popup.open()

    def open_popup_m(self):
        self.the_popup = FileChoosePopup(load=self.load_model)
        self.the_popup.filechooser.path = r"C:\Users\thoma\OneDrive - uantwerpen\tetra-autodrone\classification"
        self.the_popup.open()

    def load_model(self, selection):
        self.file_path = str(selection[0])
        self.the_popup.dismiss()
        self.lbl_train.text = "Location of loaded model is: " + self.file_path
        app = App.get_running_app()
        app.dataset_folder = self.file_path
        data = NeuralNetwork()
        data.load_model_fromh5(app.dataset_folder)
        app.network = data
        self.tab_eval.disabled = False
        self.btn_comp_train.disabled = True
        self.sld_epochs.disabled = True
        self.txt_model_name.disabled = True
        # print("Location of dataset is: ",self.file_path)
        self.tab_predict.disabled = False

    def load(self, selection):
        self.file_path = str(selection[0])
        self.the_popup.dismiss()
        self.lbl_dataset.text = "Location of dataset is: " + self.file_path
        self.btn_load.disabled = False
        app = App.get_running_app()
        app.dataset_folder = self.file_path
        # print("Location of dataset is: ",self.file_path)

    def read_data(self):
        # Clock.schedule_interval(self.console_thread_dataset, 0.5)
        # Clock.schedule_once(self.dataset_split)
        t1 = threading.Thread(target=self.dataset_split, daemon=True)
        t1.start()
        threading.Thread(target=self.console_thread_dataset, daemon=True).start()

    def dataset_split(self, *args):
        app = App.get_running_app()
        old_stdout = sys.stdout
        sys.stdout = app.mystdout = StringIO()
        old_stderr = sys.stderr
        sys.stderr = app.mystderr = StringIO()
        app = App.get_running_app()
        data = NeuralNetwork()
        data.load_folder(app.dataset_folder)
        self.lbl_dataset.text = app.mystdout.getvalue()
        data.traintestval_split()
        app.check = False
        self.lbl_dataset.text = app.mystdout.getvalue()
        data.preprocess_input()

        text = self.lbl_dataset.text
        text.join("Overzicht van de ingeladen Dataset: \n")
        text = text + "\nTrain:\n"
        text = text + str(data.train_generator.class_occ)
        text = text + "\nTest:\n"
        text = text + str(data.validation_generator.class_occ)
        text = text + "\nValidate:\n"
        text = text + str(data.test_generator.class_occ)
        self.lbl_dataset.text = text
        app.network = data
        sys.stdout = old_stdout
        random_file = np.random.randint(low=0, high=len(data.train_generator.filepaths))
        file_path = data.train_generator.filepaths[random_file]
        self.updateimage(file_path)
        self.tab_train.disabled = False
        self.txt_model_name.text = time.strftime("%Y%m%d-%H%M%S")
        self.btn_comp_train.disabled = False
        self.sld_epochs.disabled = False
        self.txt_model_name.disabled = False

    def console_thread_dataset(self):
        app = App.get_running_app()
        app.check = True
        time.sleep(1)
        while app.check:
            if app.mystderr is not None:
                console_text = app.mystderr.getvalue().splitlines()
                ind = len(console_text) - 1
                self.lbl_dataset.text = console_text[ind]

            else:
                self.lbl_dataset.text = "Preparing dataset"
            time.sleep(0.1)

    def start_learning(self):
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        app = App.get_running_app()
        app.network.create_model()
        stop = threading.Event()
        self.start_threads()
        self.btn_comp_train.text = 'Training Started'
        self.btn_comp_train.disabled = True
        self.btn_load_model.disabled = True
        # self.lbl_train.text = mystdout.getvalue()
        # sys.stdout = old_stdout

    def start_threads(self):
        threading.Thread(target=self.train_thread).start()
        threading.Thread(target=self.console_thread).start()

    def console_thread(self):
        app = App.get_running_app()
        app.check = True
        time.sleep(1)
        while app.check:
            if app.mystdout is not None:
                console_text = app.mystdout.getvalue()
                self.lbl_train.text = console_text
            else:
                self.lbl_train.text = "Training started, this might take a while..."
            time.sleep(0.1)

    def train_thread(self):
        app = App.get_running_app()
        old_stdout = sys.stdout
        sys.stdout = app.mystdout = StringIO()
        app.network.start_learning(model_name=self.txt_model_name.text,
                                   epochs=self.sld_epochs.value)
        time.sleep(1)
        app.check = False

        self.tab_eval.disabled = False
        self.btn_comp_train.disabled = False
        self.btn_load_model.disabled = True

        self.btn_comp_train.text = 'Restart training'

    def evaluate_train_network(self):
        app = App.get_running_app()
        app.network.conf_mat_plot()
        app.network.train_plot()
        self.plt_train_val.color = []
        self.plt_train_val.source = 'train_plot.png'
        self.plt_train_val.reload()
        # self.plt_train_val.color = [0, 0 ,0 , 1]
        self.plt_conf_matrix.color = []
        self.plt_conf_matrix.source = 'conf_mat.png'
        self.plt_conf_matrix.reload()

    def selected(self, filename):
        try:
            self.im_file.source = filename[0]
        except:
            pass

    def classify_images(self):

        # check if there are images in the folder
        app = App.get_running_app()
        app.network.classify_images_from_folder(self.filechrs.path)
        self.filechrs._update_files()

    def start_thread_tb(self):
        tb_thread = threading.Thread(target=self.start_tb).start()
        threading.Thread(target=self.start_browser).start()

    def start_tb(self):
        os.system("tensorboard --logdir ./logs")

    def start_browser(self):
        time.sleep(5)
        webbrowser.open('http://localhost:6006/', new=2)

    @mainthread
    def updateimage(self, imagepath):
        self.im_data.source = imagepath
        self.im_data.reload()


class NNApp(App):
    dataset_folder = None
    network = None
    check = True
    mystdout = None
    mysterr = None

    def on_stop(self):
        # The Kivy event loop is about to stop, set a stop signal;
        # otherwise the app window will close, but the Python process will
        # keep running until all secondary threads exit.
        try:
            os.system("taskkill /IM tensorboard.exe /F")
        except:
            pass

    def build(self):
        return Tab()


if __name__ == "__main__":
    NNApp().run()
