import numpy as np
from tkinter import filedialog
from tkinter import *
import split_folders
import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
import pandas as pd
import shutil
import seaborn as sns
import os

import collections
import datetime


class NeuralNetwork:
    def __init__(self):
        self.input_folder = ''
        # self.nClasses = 0
        # self.train = None
        # self.test = None
        # self.train = None
        self.test_generator = None
        self.train_generator = None
        self.validation_generator = None
        self.classweight = None
        self.model = None
        self.history = None
        self.image_size = None
        self.path = None
        self.epochs = 20
        self.batch_size = 4

    def load_folder(self, input_folder=None):
        # If input folder is
        if input_folder is None:
            root = Tk()
            root.withdraw()
            self.input_folder = filedialog.askdirectory()
        else:
            self.input_folder = input_folder
        print("Input folder is: ", self.input_folder)

    def traintestval_split(self):
        # Create a train/test/val folder in the directory.
        # If it already exists, the folder will not be updated.
        # When
        outputfolder = os.path.join(self.input_folder, 'splitfolders')

        # split_folders.ratio(inputfolder, outputfolder, seed=1337, ratio=(.8, .1, .1)) # default values
        if not os.path.exists(outputfolder):
            split_folders.ratio(self.input_folder, outputfolder, ratio=(.8, .1, .1))  # default values
            # split_folders.fixed(self.input_folder, outputfolder, fixed=(50, 50), oversample=True)  # default values
        else:
            print(
                "Warning: input folder already containts a splitfolder. "
                "\n Folder is not updated! "
                "\n If you do wish to create a new splitfolder, please delete the splitfolder")
        self.input_folder = outputfolder

    def preprocess_input(self, batch_size=4, image_height=299, image_width=299):
        self.image_size = (image_height, image_width, 3)

        def class_occurances(generator):
            # Special function to extract the number of classes and their occurances
            colt = dict(collections.Counter(generator.labels))
            keys = generator.class_indices.copy()
            for key in keys:
                ind = keys[key]
                keys[key] = colt[ind]
            return keys

        train_dir = os.path.join(self.input_folder, 'train')
        test_dir = os.path.join(self.input_folder, 'test')
        val_dir = os.path.join(self.input_folder, 'val')

        # Train images
        train_datagen = ImageDataGenerator(
            rescale=1. / 255,
            rotation_range=60,
            width_shift_range=0.4,
            height_shift_range=0.4,
            horizontal_flip=True,
            fill_mode='nearest'
            )

        train_generator = train_datagen.flow_from_directory(
            train_dir,
            target_size=(image_height, image_width),
            batch_size=batch_size,
            class_mode='categorical')
        self.train_generator = train_generator

        # Val images
        validation_datagen = ImageDataGenerator(rescale=1. / 255)
        validation_generator = validation_datagen.flow_from_directory(
            val_dir,
            target_size=(image_height, image_width),
            batch_size=batch_size,
            class_mode='categorical',
            shuffle=False)
        self.validation_generator = validation_generator

        # Test images
        test_datagen = ImageDataGenerator(rescale=1. / 255)
        test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(image_height, image_width),
            batch_size=batch_size,
            class_mode='categorical',
            shuffle=False)
        self.test_generator = test_generator

        # calculate classweights for later
        class_weights = class_weight.compute_class_weight(
            'balanced',
            np.unique(train_generator.classes),
            train_generator.classes)

        def listToDict(lst):
            op = {i: lst[i] for i in range(0, len(lst))}
            return op

        self.classweight = listToDict(class_weights)
        self.test_generator.class_occ = class_occurances(test_generator)
        self.train_generator.class_occ = class_occurances(train_generator)
        self.validation_generator.class_occ = class_occurances(validation_generator)
        self.batch_size = batch_size

    def create_model(self):
        n_classes = len(self.train_generator.class_indices)
        # create the base pre-trained model
        base_model = InceptionV3(weights='imagenet', include_top=False, input_shape=self.image_size)
        # add a global spatial average pooling layer
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        # let's add a fully-connected layer
        x = Dense(1024, activation='relu')(x)
        # and a logistic layer -- let's say we have 2 classes
        predictions = Dense(n_classes, activation='softmax')(x)
        for layer in base_model.layers:
            layer.trainable = False

        # this is the model we will train
        self.model = Model(inputs=base_model.input, outputs=predictions)


        # Compiling the model
        self.model.compile(optimizer='SGD', loss='categorical_crossentropy', metrics=['accuracy'])

    def start_learning(self, epochs=20, model_name='test_model'):
        # First train a couple of epochs on the new random layers
        self.epochs = epochs

        log_dir = os.path.join(r"logs\fit", str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S")))
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        callbacks_pre = [EarlyStopping(monitor='val_loss', patience=10, verbose=1),
                     ModelCheckpoint(model_name, verbose=1, save_best_only=True),
                     TensorBoard(log_dir=log_dir, histogram_freq=1)]
        self.history = self.model.fit(
            self.train_generator,
            steps_per_epoch=self.train_generator.samples // self.train_generator.batch_size,
            epochs=1,
            validation_data=self.validation_generator,
            validation_steps=self.validation_generator.samples // self.validation_generator.batch_size,
            verbose=1,
            callbacks=callbacks_pre,
            class_weight=self.classweight
        )
        # save classes for future evaluation use
        classes = np.array(self.test_generator.class_indices)
        np.save(os.path.join(os.getcwd(), model_name, 'classes'), classes)

        # Then train further on conv layers from inception

        for layer in self.model.layers[:249]:
            layer.trainable = False
        for layer in self.model.layers[249:]:
            layer.trainable = True

        self.model.compile(optimizer=SGD(lr=0.0001, momentum=0.9), loss='categorical_crossentropy',
                           metrics=['accuracy'])

        callbacks_tl = [EarlyStopping(monitor='val_loss', patience=10, verbose=1),
                     ModelCheckpoint(model_name, verbose=1, save_best_only=True),
                     TensorBoard(log_dir=log_dir, histogram_freq=1)]
        self.history = self.model.fit(
            self.train_generator,
            steps_per_epoch=self.train_generator.samples // self.train_generator.batch_size,
            epochs=self.epochs,
            validation_data=self.validation_generator,
            validation_steps=self.validation_generator.samples // self.validation_generator.batch_size,
            verbose=1,
            callbacks=callbacks_tl,
            class_weight=self.classweight
        )
        self.path = os.path.join(os.getcwd(), model_name)

    def load_model_fromh5(self, path_to_h5=None):
        if path_to_h5 is None:
            root = Tk()
            root.withdraw()
            path_to_h5 = filedialog.askdirectory()
        self.model = load_model(path_to_h5)
        self.path = path_to_h5
        # Only add last layers
        for layer in self.model.layers[:-3]:
            layer.trainable = False
        for layer in self.model.layers[-3:]:
            layer.trainable = True

        self.model.compile(optimizer=SGD(lr=0.0001, momentum=0.9), loss='categorical_crossentropy',
                           metrics=['accuracy'])


    def plot_wrong_images(self):
        Y_pred = self.model.predict(self.test_generator, verbose=1, steps=len(self.test_generator))
        # Every prediction higher than 0.5 will be set as true
        y_pred = np.argmax(Y_pred, axis=1)
        incorrect = (y_pred != self.test_generator.classes)
        files_wrong = np.array(self.test_generator.filepaths)[incorrect]
        inv_map = {v: k for k, v in self.test_generator.class_indices.items()}
        true_titles = [inv_map.get(i, i) for i in self.test_generator.classes[incorrect]]
        pred_titles = [inv_map.get(i, i) for i in y_pred[incorrect]]

        Nc = int(np.ceil(np.sqrt(len(files_wrong))))
        Nr = int(np.ceil(len(files_wrong) / Nc))

        fig, axs = plt.subplots(Nr, Nc)

        def load_images(image_paths):
            # Load the images from disk.
            images = [plt.imread(path) for path in image_paths]
            # Convert to a numpy array and return it.
            return np.asarray(images)

        images = load_images(files_wrong)

        for i, ax in enumerate(axs.flat):
            if i < len(files_wrong):
                ax.imshow(images[i])
                xlabel = "True: {0}\nPred: {1}".format(true_titles[i], pred_titles[i])
                ax.set_xlabel(xlabel)
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_visible(False)

            ax.set_xticks([])
            ax.set_yticks([])

        plt.show()

    def conf_mat_plot(self, save_png=True):

        # fig = plt.figure()
        # Make predictions on the test_generator
        Y_pred = self.model.predict(self.test_generator, verbose=1, steps=len(self.test_generator))
        # Every prediction higher than 0.5 will be set as true
        y_pred = np.argmax(Y_pred, axis=1)
        # preds = np.argmax(network.model.predict(network.test_generator, steps=len(network.test_generator)), axis=1)
        # Generate confusion matrix
        cm = confusion_matrix(self.test_generator.classes, y_pred)
        # Convert data and labels to use in heatmap
        list_map = list(self.test_generator.class_indices)
        df_cm = pd.DataFrame(
            cm, index=list_map, columns=list_map,
        )
        # Plot data
        figure = plt.figure(figsize=(8, 8))
        b = sns.heatmap(df_cm, annot=True, cmap=plt.cm.Blues, fmt='g', annot_kws={"size": 20}, cbar=False)
        # b.axes.set_title("Confusion Matrix", fontsize=50)
        b.set_xlabel("X Label", fontsize=30)
        b.set_ylabel("Y Label", fontsize=30)
        b.tick_params(labelsize=15)
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.show()
        if save_png is True:
            figure.savefig('conf_mat.png')

        # return figure

    def train_plot(self, save_png=True):

        # Retrieve a list of accuracy results on training and validation data
        # sets for each training epoch
        acc = self.history.history['accuracy']
        val_acc = self.history.history['val_accuracy']

        # Retrieve a list of list results on training and validation data
        # sets for each training epoch
        loss = self.history.history['loss']
        val_loss = self.history.history['val_loss']

        # Get number of epochs
        epochs = range(len(acc))

        # Plot training and validation accuracy per epoch
        e = plt.plot(epochs, acc)
        plt.plot(epochs, val_acc)
        plt.title('Training and validation accuracy',fontsize=15)

        figure = plt.figure(figsize=(8, 8))

        # Plot training and validation loss per epoch
        plt.plot(epochs, loss)
        plt.plot(epochs, val_loss)
        plt.title('Training and validation loss')
        if save_png is True:
            figure.savefig('train_plot.png')
        # return figure

    def classify_images_from_folder(self, folder):
        # get classes
        if os.path.isdir(os.path.join(folder, 'Predictions')):
            shutil.rmtree(os.path.join(folder, 'Predictions'))

        (_, image_height, image_width, _) = self.model.input.shape
        predict_datagen = ImageDataGenerator(rescale=1. / 255)
        predict_generator = predict_datagen.flow_from_directory(os.path.dirname(folder),
                                                                target_size=(image_height, image_width),
                                                                batch_size=self.batch_size,
                                                                class_mode=None,
                                                                shuffle=False,
                                                                classes=[os.path.basename(os.path.normpath(folder))])
        self.predict_generator = predict_generator

        classes = np.load(os.path.join(self.path, 'classes.npy'), allow_pickle=True)
        classes = classes.tolist()
        # Create folders

        os.mkdir(os.path.join(folder, 'Predictions'))
        for single_class in classes:
            os.mkdir(os.path.join(folder, 'Predictions', single_class))

        preds = self.model.predict(predict_generator)
        preds = np.argmax(preds, axis=1)
        classes_inv = {v: k for k, v in classes.items()}
        for i, image in enumerate(predict_generator.filepaths):
            label = preds[i]
            subfolder = classes_inv[label]
            target = os.path.join(folder, 'Predictions', subfolder)
            shutil.copyfile(image, os.path.join(target, os.path.basename(image)))
        print(preds)

