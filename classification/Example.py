from NeuralNetwork import *

# First initialise the class
network = NeuralNetwork()
# Load folder with no options will present a select folder popup
# network.load_folder(input_folder= r'C:\Users\thoma\Downloads\mnistasjpg\trainingSample\trainingSample')
network.load_folder(input_folder=r'C:\Datasets\Bird_detection\Toydataset')
# folder is split into train/test/val folders
network.traintestval_split()
# Preprocess will load the images and do the necessary preprocessing steps like augmentation, shuffling,...
network.preprocess_input()
# loading a predefined NN and adding extra layers (transfer learning)
network.create_model()
# An overview of the created model
network.model.summary()
# Start the learning process
network.start_learning(epochs=1, model_name='test_model')
# Plot a confusion matrix for the unseen test-data
network.classify_images_from_folder(r"C:\Datasets\Bird_detection\Toydataset\No_birdies")

network.conf_mat_plot()
# Plot the learning progress
network.train_plot()
# Plot the wrongly classified images.
network.plot_wrong_images()
# To see progress, start tensorboard with following cmd in the terminal:
# tensorboard --logdir ./logs


network.classify_images_from_folder(r"C:\Datasets\Bird_detection\Toydataset\No_birdies")
# The model is auto saved to the current folder, a previously trained network can be load in via:
from NeuralNetwork import *

# First initialise the class
network = NeuralNetwork()
network.load_model_fromh5(r'C:\Users\thoma\OneDrive - uantwerpen\tetra-autodrone\classification\20200414-103322')
network.classify_images_from_folder(r"C:\Datasets\Bird_detection\Toydataset\No_birdies")
