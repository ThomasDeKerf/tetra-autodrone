# Script to scrape website waarnemingen.be
import requests
from bs4 import BeautifulSoup

ctr = 0
for i in range(1, 8):
    print(i)
    URL = 'https://waarnemingen.be/photos/?after_date=2000-03-30&before_date=2020-04-06&activity=WINDFARM_VICTIM&page='
    URL = URL + str(i)
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    results = soup.findAll('img', class_='app-ratio-box-image')
    for item in results:
        image_url = item.get('src')
        image_url = image_url.split('?')[0]
        vogelstr = str(ctr) + '.jpg'
        r = requests.get(image_url, allow_redirects=True)
        open(vogelstr, 'wb').write(r.content)
        ctr += 1
