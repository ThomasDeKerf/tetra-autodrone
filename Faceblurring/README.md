### Automatic face blurring

This module uses a tensorflow object detection model from this
[repository](https://github.com/yeephycho/tensorflow-face-detection)
which has trained a mobilenet SSD(single shot multibox detector)
trained on WIDERFACE dataset. This is quite small and fast face detector
and seems to be quite effective also.

Then a simple gaussian filter is applied to all bboxes with confidence
score above a threshold (by default 0.35) and write the frames to a
video file.

it can be invoked by using:
`python3 auto_face_blurring_v2.py "path/to/video"`

Optional arguments are:
* `--score-threshold` with default value `--score-threshold=0.35` which
controls the number of bounging boxes to be blurred. All bboxes with
confidence score below this threshold are ignored.
* `--enlarge-factor` with default value `--enlarge-factor=0.1` which
controls how much the actual facial bbox will be expanded. Default value
equals to 10% for each bounding box in each direction.

Remarks:

* This module creates a new video file named _old_file_**_blurred_auto**._old_ext_.
